# lissajous

from book
go-programming-language-alan-a-a-donovan-brian-w-kernighan

* demo https://funfin-lissajous.herokuapp.com/



* local

        go build -o bin/lissajous -v .
        go install
        PORT=5005 heroku local web


* deploy to heroku

        git add . && git commit -m "Deploying to Heroku"
        heroku create funfin-lissajous
        git push heroku main
        heroku config:set PORT=8080
